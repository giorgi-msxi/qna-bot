#!/usr/bin/env python3
# Copyright (c) Microsoft Corporation. All rights reserved.
# Licensed under the MIT License.

import os


""" Bot Configuration """
# POST /knowledgebases/42bfe4ed-25ee-4256-a0a6-e414856547d0/generateAnswer
# Host: https://qna-bot-service-v1.azurewebsites.net/qnamaker
# Authorization: EndpointKey fca591f9-9235-4a0a-84dc-9a4174303360
# Content-Type: application/json
# {"question":"<Your question>"}

class DefaultConfig:
    """ Bot Configuration """

    PORT = 3978
    APP_ID =  os.environ.get("MicrosoftAppId", "") #"145e69de-b899-4b15-afbd-da13325ab423")
    APP_PASSWORD = os.environ.get("MicrosoftAppPassword", "") #"qna-chat-bot-123") 
    QNA_KNOWLEDGEBASE_ID = os.environ.get("QnAKnowledgebaseId", "42bfe4ed-25ee-4256-a0a6-e414856547d0") 
    QNA_ENDPOINT_KEY = os.environ.get("QnAEndpointKey", "fca591f9-9235-4a0a-84dc-9a4174303360") 
    QNA_ENDPOINT_HOST = os.environ.get("QnAEndpointHostName", "https://qna-bot-service-v1.azurewebsites.net/qnamaker") 
