﻿# QnA bot Implementation steps:

This package is created using QnA code sample from:
https://github.com/microsoft/BotBuilder-Samples/tree/main/samples/python

To run locally:
python app.py

# Azure deployment steps:

1.	Create Knowledge Database – from qnamaker.ai
Extract information from “My knowledge bases” -> “View code”, which to be used: config.py

2.	Azure application registration:
az ad app create --display-name "qna-chat-bot" --password "qna-chat-bot-123" --available-to-other-tenants

3.	Deploy ARM template with pre-existing resource group:
az deployment group create --resource-group "rg-mw-dev" --template-file "deploymentTemplates/template-with-preexisting-rg.json"\
     --parameters appId="145e69de-b899-4b15-afbd-da13325ab423" appSecret="qna-chat-bot-123" botId="qna-chat-bot-id"\
     newWebAppName="qna-chat-bot-service" newAppServicePlanName="qna-chat-bot-plan" --name "qna-chat-bot-service"\
     appServicePlanLocation="UK South"

4.	Deploy code to Azure, as a zip folder. Note, zip folder shouldn’t contain top directory,
az webapp deployment source config-zip --resource-group "rg-mw-dev" --name "qna-chat-bot-service" --src "qna-bot.zip"

5.	Configure App Service (qna-chat-bot-service) to sync with Gitlab repo




